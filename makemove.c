// makemove.c

#include "defs.h"

// Define some hashing macros for convenience.
// Here we suppose that `pos` exists in our program.
#define HASH_PCE(pce, sq) (pos->posKey ^= (PieceKeys[(pce)][(sq)]))
#define HASH_CA (pos->posKey ^= (CastleKeys[(pos->castlePerm)]))
#define HASH_SIDE (pos->posKey ^= (SideKey))
#define HASH_EP (pos->posKey ^= (PieceKeys[EMPTY][(pos->enPas)]))

// We will use this array to easily compute castling permissions.
// We will do that by using an AND operator with the square `from` and `to`.
// This works because we are using 4 bits to maintain castling permissions.
// So in most of the squares the result of the `AND` operation will be a 15, 
// except from the squares marked below with 7, 3, 11 for the black pieces and
// 13, 12, 14 for the white pieces.
// So suppose that the black king moves from e8, which is a three
// ca_perm &= 3, before that the castle permission was 1111, but it will now be
// due to the `AND` operator 0011, so we would have lost castling permissions on both 
// the king and queen side. That is why we have chosen these values for this fixed 
// constant array
const int CastlePerm[120] = {
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 13, 15, 15, 15, 12, 15, 15, 14, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 7, 15, 15, 15, 3, 15, 15, 11, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
        15, 15, 15, 15, 15, 15, 15, 15, 15, 15
};


static void ClearPiece(const int sq, S_BOARD *pos) {
    ASSERT(SqOnBoard(sq));        // Assert that the square is on the board.
    ASSERT(CheckBoard(pos));    // Assert that the position is correct.

    int pce = pos->pieces[sq];    // Get the piece residing on this position.

    ASSERT(PieceValid(pce));    // Assert that the piece is a valid piece.

    int col = PieceCol[pce];    // Get the color of the piece.
    int index = 0;                // Simple variable used for looping.
    int t_pceNum = -1;            // Used in the piece list.

    ASSERT(SideValid(col));        // Assert that the piece belongs to a valid side.

    HASH_PCE(pce,
             sq);            // We XOR this piece from the position key. This is equal to removing it from the hash key of this position.

    pos->pieces[sq] = EMPTY;    // We set the square to empty.
    pos->material[col] -= PieceVal[pce];    // And subtract its value from the material score of this color.

    if (PieceBig[pce]) {            // If the piece is a big piece
        pos->bigPce[col]--;    // adjust the big piece counter.
        if (PieceMaj[pce]) {        // Same for major
            pos->majPce[col]--;
        } else {                // and minor pieces.
            pos->minPce[col]--;
        }
    } else {
        CLRBIT(pos->pawns[col], SQ64(sq)); // We now clear the bit both from the bitboard of this color
        CLRBIT(pos->pawns[BOTH], SQ64(sq));// and the birboard of both colors using the 64 notation.
    }

    // We will now effectively remove this piece from the piece list.
    // Suppose we are dealing with white pawns and only have 5.
    // pos->pceNum[wP] would return five and thus the loop would be from 0 to 4.
    // We then check if one of those pieces in this piece list is equal with our piece.
    // If that was square 3, then t_pceNum would become 3, the value of the index
    // of that piece.
    for (index = 0; index < pos->pceNum[pce]; ++index) {
        if (pos->pList[pce][index] == sq) {
            t_pceNum = index;
            break;
        }
    }

    ASSERT(t_pceNum != -1); // This would mean that the piece and the pieces array dont match up.
    ASSERT(t_pceNum >= 0 && t_pceNum < 10); // In general you cant have more thatn 10 pieces of a kind.

    // We then immediately decrement the piece number,
    // making it in our example a 4.
    pos->pceNum[pce]--;

    // We now subsitute what we had in piece 3, or else we substitute the piece we got as
    // an argument here, with the piece at number 4, effectively removing that piece from
    // the board.
    pos->pList[pce][t_pceNum] = pos->pList[pce][pos->pceNum[pce]];
}

// Add the piece pce at position sq using this poition pointer pos.
// This is identical to the remove piece method, instead that this time
// we add and do not take away from the various arrays.
static void AddPiece(const int sq, S_BOARD *pos, const int pce) {
    ASSERT(PieceValid(pce));
    ASSERT(SqOnBoard(sq));

    int col = PieceCol[pce];
    ASSERT(SideValid(col));

    HASH_PCE(pce, sq);

    pos->pieces[sq] = pce;

    if (PieceBig[pce]) {
        pos->bigPce[col]++;
        if (PieceMaj[pce]) {
            pos->majPce[col]++;
        } else {
            pos->minPce[col]++;
        }
    } else {
        SETBIT(pos->pawns[col], SQ64(sq));
        SETBIT(pos->pawns[BOTH], SQ64(sq));
    }

    pos->material[col] += PieceVal[pce];
    pos->pList[pce][pos->pceNum[pce]++] = sq; // First use this index and then increment.

}

// Move a piece from square `from` to square `to`
static void MovePiece(const int from, const int to, S_BOARD *pos) {
    ASSERT(SqOnBoard(from));
    ASSERT(SqOnBoard(to));

    int index = 0;
    int pce = pos->pieces[from];
    int col = PieceCol[pce];
    ASSERT(SideValid(col));
    ASSERT(PieceValid(pce));

#ifdef DEBUG
    int t_PieceNum = FALSE;
#endif

    HASH_PCE(pce, from);
    pos->pieces[from] = EMPTY;

    HASH_PCE(pce, to);
    pos->pieces[to] = pce;

    if (!PieceBig[pce]) {
        CLRBIT(pos->pawns[col], SQ64(from));
        CLRBIT(pos->pawns[BOTH], SQ64(from));
        SETBIT(pos->pawns[col], SQ64(to));
        SETBIT(pos->pawns[BOTH], SQ64(to));
    }

    for (index = 0; index < pos->pceNum[pce]; ++index) {
        if (pos->pList[pce][index] == from) {
            pos->pList[pce][index] = to;
#ifdef DEBUG
            t_PieceNum = TRUE;
#endif
            break;
        }
    }
    ASSERT(t_PieceNum);
}

int MakeMove(S_BOARD *pos, int move) {

    ASSERT(CheckBoard(pos));

    int from = FROMSQ(move);
    int to = TOSQ(move);
    int side = pos->side;

    ASSERT(SqOnBoard(from));
    ASSERT(SqOnBoard(to));
    ASSERT(SideValid(side));
    ASSERT(PieceValid(pos->pieces[from]));
    ASSERT(pos->hisPly >= 0 && pos->hisPly < MAXGAMEMOVES);
    ASSERT(pos->ply >= 0 && pos->ply < MAXDEPTH);

    pos->history[pos->hisPly].posKey = pos->posKey;

    if (move & MFLAGEP) { // If it was an en passant move
        if (side == WHITE) { // and the white side was moving
            ClearPiece(to - 10, pos); // remove the piece here.
        } else {
            ClearPiece(to + 10, pos);
        }
    } else if (move & MFLAGCA) { // Caslting move
        switch (to) {
            case C1:
                MovePiece(A1, D1, pos);
                break;
            case C8:
                MovePiece(A8, D8, pos);
                break;
            case G1:
                MovePiece(H1, F1, pos);
                break;
            case G8:
                MovePiece(H8, F8, pos);
                break;
            default:
                ASSERT(FALSE);
                break;
        }
    }

    if (pos->enPas != NO_SQ) HASH_EP;
    HASH_CA; // we hash out the caslint state

    pos->history[pos->hisPly].move = move;
    pos->history[pos->hisPly].fiftyMove = pos->fiftyMove;
    pos->history[pos->hisPly].enPas = pos->enPas;
    pos->history[pos->hisPly].castlePerm = pos->castlePerm;

    pos->castlePerm &= CastlePerm[from];
    pos->castlePerm &= CastlePerm[to];
    pos->enPas = NO_SQ;

    HASH_CA; // we hash it back in again after the new suggestion

    int captured = CAPTURED(move);
    pos->fiftyMove++;

    if (captured != EMPTY) {
        ASSERT(PieceValid(captured));
        ClearPiece(to, pos);
        pos->fiftyMove = 0;
    }

    pos->hisPly++;
    pos->ply++;

    ASSERT(pos->hisPly >= 0 && pos->hisPly < MAXGAMEMOVES);
    ASSERT(pos->ply >= 0 && pos->ply < MAXDEPTH);

    if (PiecePawn[pos->pieces[from]]) { //Check if we have a new en passant
        pos->fiftyMove = 0;
        if (move & MFLAGPS) {
            if (side == WHITE) {
                pos->enPas = from + 10;
                ASSERT(RanksBrd[pos->enPas] == RANK_3);
            } else {
                pos->enPas = from - 10;
                ASSERT(RanksBrd[pos->enPas] == RANK_6);
            }
            HASH_EP; // We hash in this new en passant square
        }
    }

    MovePiece(from, to, pos);

    int prPce = PROMOTED(move);
    if (prPce != EMPTY) {
        ASSERT(PieceValid(prPce) && !PiecePawn[prPce]); // We cant promote to a pawn
        ClearPiece(to, pos); // firt clear then add, obviously
        AddPiece(to, pos, prPce);
    }

    if (PieceKing[pos->pieces[to]]) {
        pos->KingSq[pos->side] = to;
    }

    pos->side ^= 1; // Change side.
    HASH_SIDE;

    ASSERT(CheckBoard(pos));

    if (SqAttacked(pos->KingSq[side], pos->side,
                   pos)) { // If the side that did the move, is attacked by the new side and is a king pice, you cant do that move.
        TakeMove(pos);
        return FALSE;
    }

    return TRUE;
}

// In short we do whatever make mover did, only backwards.
void TakeMove(S_BOARD *pos) {

    ASSERT(CheckBoard(pos));

    pos->hisPly--;
    pos->ply--;

    ASSERT(pos->hisPly >= 0 && pos->hisPly < MAXGAMEMOVES);
    ASSERT(pos->ply >= 0 && pos->ply < MAXDEPTH);

    int move = pos->history[pos->hisPly].move;
    int from = FROMSQ(move);
    int to = TOSQ(move);

    ASSERT(SqOnBoard(from));
    ASSERT(SqOnBoard(to));

    if (pos->enPas != NO_SQ) HASH_EP; // hash out en passant and castling
    HASH_CA;

    pos->castlePerm = pos->history[pos->hisPly].castlePerm;
    pos->fiftyMove = pos->history[pos->hisPly].fiftyMove;
    pos->enPas = pos->history[pos->hisPly].enPas;

    if (pos->enPas != NO_SQ) HASH_EP; // Hash them back in if they changed
    HASH_CA;

    pos->side ^= 1;
    HASH_SIDE;

    if (MFLAGEP & move) { // Give the piece captured by en passant back
        if (pos->side == WHITE) {
            AddPiece(to - 10, pos, bP);
        } else {
            AddPiece(to + 10, pos, wP);
        }
    } else if (MFLAGCA & move) { // Move the rook bavk if it was a castling move
        switch (to) {
            case C1:
                MovePiece(D1, A1, pos);
                break;
            case C8:
                MovePiece(D8, A8, pos);
                break;
            case G1:
                MovePiece(F1, H1, pos);
                break;
            case G8:
                MovePiece(F8, H8, pos);
                break;
            default:
                ASSERT(FALSE);
                break;
        }
    }

    MovePiece(to, from, pos);

    if (PieceKing[pos->pieces[from]]) {
        pos->KingSq[pos->side] = from;
    }

    int captured = CAPTURED(move);
    if (captured != EMPTY) {
        ASSERT(PieceValid(captured));
        AddPiece(to, pos, captured);
    }

    if (PROMOTED(move) != EMPTY) {
        ASSERT(PieceValid(PROMOTED(move)) && !PiecePawn[PROMOTED(move)]);
        ClearPiece(from, pos);
        AddPiece(from, pos, (PieceCol[PROMOTED(move)] == WHITE ? wP : bP));
    }

    ASSERT(CheckBoard(pos));
}


void MakeNullMove(S_BOARD *pos) {

    ASSERT(CheckBoard(pos));
    ASSERT(!SqAttacked(pos->KingSq[pos->side], pos->side ^ 1, pos));

    pos->ply++;
    pos->history[pos->hisPly].posKey = pos->posKey;

    if (pos->enPas != NO_SQ) HASH_EP;

    pos->history[pos->hisPly].move = NOMOVE;
    pos->history[pos->hisPly].fiftyMove = pos->fiftyMove;
    pos->history[pos->hisPly].enPas = pos->enPas;
    pos->history[pos->hisPly].castlePerm = pos->castlePerm;
    pos->enPas = NO_SQ;

    pos->side ^= 1;
    pos->hisPly++;
    HASH_SIDE;

    ASSERT(CheckBoard(pos));
    ASSERT(pos->hisPly >= 0 && pos->hisPly < MAXGAMEMOVES);
    ASSERT(pos->ply >= 0 && pos->ply < MAXDEPTH);

    return;
} // MakeNullMove

void TakeNullMove(S_BOARD *pos) {
    ASSERT(CheckBoard(pos));

    pos->hisPly--;
    pos->ply--;

    if (pos->enPas != NO_SQ) HASH_EP;

    pos->castlePerm = pos->history[pos->hisPly].castlePerm;
    pos->fiftyMove = pos->history[pos->hisPly].fiftyMove;
    pos->enPas = pos->history[pos->hisPly].enPas;

    if (pos->enPas != NO_SQ) HASH_EP;
    pos->side ^= 1;
    HASH_SIDE;

    ASSERT(CheckBoard(pos));
    ASSERT(pos->hisPly >= 0 && pos->hisPly < MAXGAMEMOVES);
    ASSERT(pos->ply >= 0 && pos->ply < MAXDEPTH);
}
